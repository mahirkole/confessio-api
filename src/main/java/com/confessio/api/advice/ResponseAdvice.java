package com.confessio.api.advice;

import com.confessio.api.core.CoreResponse;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ResponseAdvice {

    @Around("execution(* com.confessio.api.controller.*.*(..))")
    public ResponseEntity response(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        ResponseEntity responseEntity = (ResponseEntity) proceedingJoinPoint.proceed();
        return new ResponseEntity<>(new CoreResponse(responseEntity.getBody()), responseEntity.getStatusCode());
    }

}
