package com.confessio.api.advice;

import com.confessio.api.core.CoreAdvice;
import com.confessio.api.core.CoreException;
import com.confessio.api.core.CoreResponse;
import com.confessio.api.core.Error;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ExceptionAdvice extends CoreAdvice {

    @Around("execution(* com.confessio.api.controller.*.*(..))")
    public ResponseEntity handle(ProceedingJoinPoint proceedingJoinPoint) {
        ResponseEntity responseEntity;

        try {
            responseEntity = (ResponseEntity) proceedingJoinPoint.proceed();
        } catch (CoreException coreException) {
            return coreException.handle();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return new ResponseEntity<>(
                    new CoreResponse(false, new Error(throwable.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

}
