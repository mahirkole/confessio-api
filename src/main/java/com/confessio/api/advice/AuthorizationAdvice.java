package com.confessio.api.advice;

import com.confessio.api.core.CoreAdvice;
import com.confessio.api.core.CoreDto;
import com.confessio.api.service.UserService;
import com.confessio.api.util.BannedUserStrategy;
import com.confessio.api.util.IsBanned;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;

import java.lang.reflect.Method;

//@Aspect
public class AuthorizationAdvice extends CoreAdvice implements ApplicationContextAware {

    private final UserService userService;
    private final Authentication authentication;

    private ApplicationContext applicationContext;

    @Autowired
    public AuthorizationAdvice(UserService userService, Authentication authentication) {
        this.userService = userService;
        this.authentication = authentication;
    }

    @Around("execution(* com.confessio.api.service.*.*(..)) && @annotation(com.confessio.api.util.IsBanned)")
    public CoreDto handleUserAuth(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = methodSignature.getMethod();
        IsBanned isBannedAnnotation = method.getAnnotation(IsBanned.class);

        Long authUserId = Long.parseLong(this.authentication.getCredentials().toString());
        BannedUserStrategy strategy = (BannedUserStrategy) applicationContext.getBean(isBannedAnnotation.strategy());

        int parameterOrder = 0;

        for (int i = 0; i < methodSignature.getParameterNames().length; i++) {
            if (methodSignature.getParameterNames()[i].equals(isBannedAnnotation.name())) {
                parameterOrder = i;
                break;
            }
        }

        if (!isBannedAnnotation.id_field().equals("")) {
            Long parameter = (Long) proceedingJoinPoint.getArgs()[parameterOrder];
            strategy.handle(parameter, authUserId);
        }

        return (CoreDto) proceedingJoinPoint.proceed();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
