package com.confessio.api.advice;

import com.confessio.api.core.CoreResponse;
import com.confessio.api.core.Error;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handle(MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(
                new CoreResponse(false, getErrorListForBindingResults(ex.getBindingResult())),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity handle(HttpMessageNotReadableException ex) {
        List<Error> errorList = new ArrayList<>();
        errorList.add(new Error(ex.getMessage()));
        return new ResponseEntity<>(new CoreResponse(false, errorList), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity handle(BindException ex) {
        return new ResponseEntity<>(
                new CoreResponse(false, getErrorListForBindingResults(ex.getBindingResult())),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity handle(HttpRequestMethodNotSupportedException ex) {
        return new ResponseEntity<>(
                new CoreResponse(false, new Error("Method not allowed")),
                HttpStatus.METHOD_NOT_ALLOWED);
    }

    private List<Error> getErrorListForBindingResults(BindingResult bindingResult) {
        List<Error> errorList = new ArrayList<>();

        bindingResult.getAllErrors().forEach((bindingError) -> {
            Error error = new Error();
            String fieldName = ((FieldError) bindingError).getField();
            String errorMessage = bindingError.getDefaultMessage();
            error.setErrorMessage("[" + fieldName + "] : " + errorMessage);
            error.addParameter(fieldName, errorMessage);
            errorList.add(error);
        });

        return errorList;
    }
}
