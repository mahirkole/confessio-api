package com.confessio.api.dto;

import com.confessio.api.core.CoreDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class CityDto extends CoreDto {

    private Long id;

    @NotBlank
    @Size(min = 3, max = 20)
    private String name;
}
