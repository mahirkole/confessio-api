package com.confessio.api.dto;

import com.confessio.api.core.CoreDto;
import com.confessio.api.model.Comment;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class CommentDto extends CoreDto {

    private Long id;
    @NotNull
    private Long userId;
    @NotNull
    private Long postId;

    @NotBlank
    @Size(max = 400)
    private String content;

    private Long parentId;

    private List<CommentDto> children;
}
