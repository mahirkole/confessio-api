package com.confessio.api.dto;

import com.confessio.api.core.CoreDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class HeartDto extends CoreDto {

    @NotNull
    private Long userId;
    @NotNull
    private Long postId;
}
