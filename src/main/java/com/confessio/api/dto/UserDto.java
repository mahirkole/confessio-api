package com.confessio.api.dto;

import com.confessio.api.core.CoreDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class UserDto extends CoreDto {

    private Long id;

    @NotBlank
    @Size(min = 4, max = 16)
    private String username;

    @Size(min = 4, max = 32)
    private String password;

    @Range(min = 0, max = 1)
    private Short isActive = 1;

    private String role;
}
