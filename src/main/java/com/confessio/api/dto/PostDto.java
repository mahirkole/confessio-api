package com.confessio.api.dto;

import com.confessio.api.core.CoreDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class PostDto extends CoreDto {

    private Long id;
    private Long userId;

    @NotNull
    private Long locationId;

    @NotBlank
    private String content;
}
