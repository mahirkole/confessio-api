package com.confessio.api.dto;

import com.confessio.api.core.CoreDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class LocationDto extends CoreDto {

    private Long id;
    @NotBlank
    private String name;
    @NotNull
    private Long cityId;
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
}
