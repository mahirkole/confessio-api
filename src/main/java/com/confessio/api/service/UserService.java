package com.confessio.api.service;

import com.confessio.api.dto.UserDto;
import com.confessio.api.exception.CannotBanYourselfException;
import com.confessio.api.exception.UserNotFoundException;
import com.confessio.api.exception.UsernameIsTakenException;
import com.confessio.api.mapper.UserMapper;
import com.confessio.api.model.User;
import com.confessio.api.model.UserBan;
import com.confessio.api.repository.UserRepository;
import com.confessio.api.service.util.FinderUtil;
import com.confessio.api.util.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    private final FinderUtil finderUtil;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserService(FinderUtil finderUtil, UserRepository userRepository, UserMapper userMapper, BCryptPasswordEncoder passwordEncoder) {
        this.finderUtil = finderUtil;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto getUserByUsername(String username) throws UserNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        user.setPassword(null);
        return userMapper.toDto(user);
    }

    public UserDto getUserById(Long id, Long authUserId) throws UserNotFoundException {
        if (isUserBannedByBannerUser(authUserId, id)) {
            throw new UserNotFoundException(id);
        }
        return userMapper.toDto(finderUtil.getUserById(id));
    }

    public UserDto createUser(@Valid UserDto userDto) throws UsernameIsTakenException {
        userDto.setId(null);
        User user = userMapper.toModel(userDto);

        if (isUserExists(user.getUsername())) {
            throw new UsernameIsTakenException(user.getUsername());
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userMapper.toDto(userRepository.save(user));
    }

    private boolean isUserExists(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user =
                userRepository
                        .findByUsername(username)
                        .orElseThrow(() -> new UsernameNotFoundException(username));
        return new UserDetailsImpl(user);
    }

    public void banUnbanUser(Long userId, Long authUserId) throws UserNotFoundException, CannotBanYourselfException {
        if (authUserId.equals(userId)) {
            throw new CannotBanYourselfException();
        }

        User banned = finderUtil.getUserById(userId);
        User banner = finderUtil.getUserById(authUserId);

        /*
        if (banner.getBannedUsers().contains(banned)) {
            banner.unbanUser(banned);
        } else {
            banner.banUser(banned);
        }
        */

        if(banner.getUserBans().contains(new UserBan(banner, banned))) {
            banner.unbanUser(banned);
        } else {
            banner.banUser(banned);
        }

        userRepository.save(banner);
    }

    public Page<UserDto> getBannedUsersOfUserId(Long authUserId, int pageNumber) throws UserNotFoundException {
        User user = finderUtil.getUserById(authUserId);
        return userRepository.findUsersBannedBy(user.getId(), PageRequest.of(pageNumber, 1)).map(userMapper::toDto);
    }

    public List<UserDto> getAllUsers() {
        return null;
        //return userMapper.toDto(userRepository.getAllUsers());
    }

    public Boolean isUserBannedByBannerUser(Long userId, Long bannerUserId) throws UserNotFoundException {
        User banned = finderUtil.getUserById(userId);
        User banner = finderUtil.getUserById(bannerUserId);

        //return banner.getBannedUsers().contains(banned);
        return banner.getUserBans().contains(new UserBan(banner, banned));
    }
}
