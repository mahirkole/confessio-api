package com.confessio.api.service;

import com.confessio.api.dto.CityDto;
import com.confessio.api.dto.LocationDto;
import com.confessio.api.exception.CityNotFoundException;
import com.confessio.api.exception.LocationNotFoundException;
import com.confessio.api.mapper.CityMapper;
import com.confessio.api.mapper.LocationMapper;
import com.confessio.api.model.City;
import com.confessio.api.model.Location;
import com.confessio.api.repository.CityRepository;
import com.confessio.api.repository.LocationRepository;
import com.confessio.api.service.util.FinderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class LocationService {

    private final FinderUtil finderUtil;

    private final LocationRepository locationRepository;
    private final CityRepository cityRepository;

    private final LocationMapper locationMapper;
    private final CityMapper cityMapper;

    @Autowired
    public LocationService(FinderUtil finderUtil,
                           LocationRepository locationRepository,
                           CityRepository cityRepository,
                           LocationMapper locationMapper,
                           CityMapper cityMapper) {
        this.finderUtil = finderUtil;
        this.locationRepository = locationRepository;
        this.cityRepository = cityRepository;
        this.locationMapper = locationMapper;
        this.cityMapper = cityMapper;
    }

    public LocationDto getLocationById(Long id) throws LocationNotFoundException {
        return locationMapper.toDto(finderUtil.getLocationById(id));
    }

    public CityDto getCityById(Long id) throws CityNotFoundException {
        return cityMapper.toDto(finderUtil.getCityById(id));
    }

    public Page<LocationDto> getLocationsByCityId(Long cityId, int pageNumber) {
        return locationRepository.findByCity(cityId, PageRequest.of(pageNumber, 1)).map(locationMapper::toDto);
    }

    public Page<LocationDto> getLocations(int pageNumber) {
        return locationRepository.findAll(PageRequest.of(pageNumber, 1)).map(locationMapper::toDto);
    }

    public LocationDto createLocation(LocationDto locationDto) throws CityNotFoundException {
        if (!cityRepository.findById(locationDto.getCityId()).isPresent()) {
            throw new CityNotFoundException(locationDto.getCityId());
        }

        Location location = locationMapper.toModel(locationDto);
        return locationMapper.toDto(locationRepository.save(location));
    }

    public LocationDto updateLocation(LocationDto locationDto) throws LocationNotFoundException, CityNotFoundException {
        Location location = finderUtil.getLocationById(locationDto.getId());

        boolean needToSave = false;

        if (locationDto.getName() != null) {
            location.setName(locationDto.getName());
            needToSave = true;
        }

        if (locationDto.getCityId() != null && !location.getCity().getId().equals(locationDto.getCityId())) {
            City city = finderUtil.getCityById(locationDto.getCityId());

            location.setCity(city);
            needToSave = true;
        }

        if (needToSave) {
            return locationMapper.toDto(locationRepository.save(location));
        } else {
            return locationMapper.toDto(location);
        }
    }

    public CityDto updateCity(CityDto cityDto) throws CityNotFoundException {
        City city = finderUtil.getCityById(cityDto.getId());

        boolean needToSave = false;

        if (cityDto.getName() != null) {
            city.setName(cityDto.getName());
            needToSave = true;
        }

        if (needToSave) {
            return cityMapper.toDto(cityRepository.save(city));
        } else {
            return cityMapper.toDto(city);
        }
    }

    public Page<CityDto> getCities(int pageNumber) {
        return cityRepository.findAll(PageRequest.of(pageNumber, 1)).map(cityMapper::toDto);
    }
}
