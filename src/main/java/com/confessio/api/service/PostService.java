package com.confessio.api.service;

import com.confessio.api.dto.CommentDto;
import com.confessio.api.dto.HeartDto;
import com.confessio.api.dto.PostDto;
import com.confessio.api.exception.*;
import com.confessio.api.mapper.CommentMapper;
import com.confessio.api.mapper.HeartMapper;
import com.confessio.api.mapper.PostMapper;
import com.confessio.api.model.*;
import com.confessio.api.repository.CommentRepository;
import com.confessio.api.repository.HeartRepository;
import com.confessio.api.repository.PostRepository;
import com.confessio.api.repository.UserRepository;
import com.confessio.api.service.util.FinderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PostService {

    private final FinderUtil finderUtil;

    private final UserService userService;

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final HeartRepository heartRepository;
    private final UserRepository userRepository;

    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final HeartMapper heartMapper;

    @Autowired
    public PostService(FinderUtil finderUtil,
                       UserService userService,
                       PostRepository postRepository,
                       CommentRepository commentRepository,
                       HeartRepository heartRepository,
                       UserRepository userRepository,
                       PostMapper postMapper,
                       CommentMapper commentMapper,
                       HeartMapper heartMapper) {
        this.finderUtil = finderUtil;
        this.userService = userService;
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.heartRepository = heartRepository;
        this.userRepository = userRepository;
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
        this.heartMapper = heartMapper;
    }

    public Page<CommentDto> getCommentsOfPostByPostId(Long id, Long authUserId, int pageNumber)
            throws PostNotFoundException, UserNotFoundException {
        Post post = finderUtil.getPostById(id);

        if (userService.isUserBannedByBannerUser(authUserId, post.getUser().getId())) {
            throw new PostNotFoundException(id);
        }

        return commentRepository.getAllByPostId(id, PageRequest.of(pageNumber, 1)).map(commentMapper::toDto);
    }

    public CommentDto createComment(@Valid CommentDto commentDto)
            throws PostNotFoundException, ParentCommentNotFoundException, PostForCommentIsInconsistentException,
            UserNotFoundException {
        commentDto.setId(null);
        Comment comment = commentMapper.toModel(commentDto);

        Optional<Post> optionalPost = postRepository.findById(commentDto.getPostId());

        if (!optionalPost.isPresent()) {
            throw new PostNotFoundException(commentDto.getPostId());
        }

        if (userService.isUserBannedByBannerUser(commentDto.getUserId(), optionalPost.get().getUser().getId())) {
            throw new PostNotFoundException(commentDto.getPostId());
        }

        comment.setPost(optionalPost.get());

        if (commentDto.getParentId() != null) {
            Optional<Comment> optionalParentComment = commentRepository.findById(commentDto.getParentId());

            if (!optionalParentComment.isPresent()) {
                throw new ParentCommentNotFoundException(commentDto.getParentId());
            }

            if (!Objects.equals(optionalParentComment.get().getPost().getId(), commentDto.getPostId())) {
                throw new PostForCommentIsInconsistentException(commentDto.getPostId());
            }

            comment.setParent(optionalParentComment.get());
        }

        comment.setUser(finderUtil.getUserById(commentDto.getUserId()));

        return commentMapper.toDto(commentRepository.save(comment));
    }

    public PostDto createPost(@Valid PostDto postDto) throws LocationNotFoundException, UserNotFoundException {
        postDto.setId(null);
        Post post = postMapper.toModel(postDto);
        post.setUser(finderUtil.getUserById(postDto.getUserId()));
        post.setLocation(finderUtil.getLocationById(postDto.getLocationId()));
        return postMapper.toDto(postRepository.save(post));
    }

    //@IsBanned(strategy = BannedUserPostAccessStrategy.class, id_field = "id")
    public PostDto getPostById(Long id, Long authUserId) throws PostNotFoundException, UserNotFoundException {
        Post post = postRepository.findById(id).orElseThrow(() -> new PostNotFoundException(id));
        if (userService.isUserBannedByBannerUser(authUserId, post.getUser().getId())) {
            throw new PostNotFoundException(id);
        }
        return postMapper.toDto(post);
    }

    public HeartDto heartPost(Long id, Long authUserId)
            throws PostNotFoundException, UserNotFoundException {
        Post post = finderUtil.getPostById(id);

        if (userService.isUserBannedByBannerUser(authUserId, post.getUser().getId())) {
            throw new PostNotFoundException(id);
        }

        User user = finderUtil.getUserById(authUserId);
        Optional<Heart> optionalHeart = heartRepository.findByPostIdAndUserId(id, authUserId);

        if (optionalHeart.isPresent()) {
            return heartMapper.toDto(optionalHeart.get());
        } else {
            Heart heart = new Heart();
            heart.setPost(post);
            heart.setUser(user);
            return heartMapper.toDto(heartRepository.save(heart));
        }
    }

    public List<HeartDto> getHeartsOfPostByPostId(Long id, Long authUserId)
            throws PostNotFoundException, UserNotFoundException {
        Post post = finderUtil.getPostById(id);
        if (userService.isUserBannedByBannerUser(authUserId, post.getUser().getId())) {
            throw new PostNotFoundException(id);
        }
        return heartMapper.toDto(heartRepository.getAllByPostId(post.getId()));
    }

    public void unheartPost(Long id, Long authUserId)
            throws PostNotFoundException, HeartNotFoundException, UserNotFoundException {
        Post post = finderUtil.getPostById(id);

        if (userService.isUserBannedByBannerUser(authUserId, post.getUser().getId())) {
            throw new PostNotFoundException(id);
        }

        if (!userRepository.findById(authUserId).isPresent()) {
            throw new UserNotFoundException(authUserId);
        }

        Optional<Heart> optionalHeart = heartRepository.findByPostIdAndUserId(id, authUserId);

        if (optionalHeart.isPresent()) {
            heartRepository.delete(optionalHeart.get());
        } else {
            throw new HeartNotFoundException();
        }
    }

    public Page<PostDto> getPostsByLocationId(Long locationId, Long authUserId, int pageNumber)
            throws LocationNotFoundException {
        Location location = finderUtil.getLocationById(locationId);
        return postRepository
                .getAllByLocationId(location.getId(), authUserId, PageRequest.of(pageNumber, 1))
                .map(postMapper::toDto);
    }

    public Page<PostDto> getAll(Long authUserId, int pageNumber) {
        return postRepository.getAll(authUserId, PageRequest.of(pageNumber, 10)).map(postMapper::toDto);
    }

    public Page<PostDto> getPostsByCityId(Long cityId, Long authUserId, int pageNumber) throws CityNotFoundException {
        City city = finderUtil.getCityById(cityId);
        return postRepository
                .getAllByCityId(city.getId(), authUserId, PageRequest.of(pageNumber, 1))
                .map(postMapper::toDto);
    }

    public void deletePostById(Long id, Long authUserId) throws PostNotFoundException, ForbiddenActionException {
        Post post = finderUtil.getPostById(id);

        if (authUserId.equals(post.getUser().getId())) {
            postRepository.delete(post);
        } else {
            throw new ForbiddenActionException();
        }
    }

    public Page<PostDto> getPostsByUserId(Long userId, Long authUserId, int pageNumber) throws UserNotFoundException {
        if (userService.isUserBannedByBannerUser(authUserId, userId)) {
            throw new UserNotFoundException(userId);
        }

        return postRepository.getAllByUserId(userId, PageRequest.of(pageNumber, 1)).map(postMapper::toDto);
    }
}
