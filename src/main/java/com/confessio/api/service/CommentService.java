package com.confessio.api.service;

import com.confessio.api.dto.CommentDto;
import com.confessio.api.dto.PostDto;
import com.confessio.api.exception.*;
import com.confessio.api.mapper.CommentMapper;
import com.confessio.api.model.Comment;
import com.confessio.api.repository.CommentRepository;
import com.confessio.api.service.util.FinderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    private final FinderUtil finderUtil;
    private final PostService postService;
    private final UserService userService;
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;

    @Autowired
    public CommentService(FinderUtil finderUtil,
                          PostService postService,
                          UserService userService,
                          CommentRepository commentRepository,
                          CommentMapper commentMapper) {
        this.finderUtil = finderUtil;
        this.postService = postService;
        this.userService = userService;
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
    }

    public CommentDto getCommentById(Long id, Long authUserId) throws CommentNotFoundException, UserNotFoundException {
        Comment comment = finderUtil.getCommentById(id);

        if (userService.isUserBannedByBannerUser(authUserId, comment.getUser().getId())) {
            throw new CommentNotFoundException(id);
        }

        return commentMapper.toDto(comment);
    }

    public List<CommentDto> getChildren(Long parentId, Long authUserId)
            throws ParentCommentNotFoundException, CommentIsNotParentException, UserNotFoundException {
        Comment comment = commentRepository
                .findById(parentId)
                .orElseThrow(() -> new ParentCommentNotFoundException(parentId));

        if (userService.isUserBannedByBannerUser(authUserId, comment.getUser().getId())) {
            throw new ParentCommentNotFoundException(parentId);
        }

        if (comment.getParent() != null) {
            throw new CommentIsNotParentException(parentId);
        }

        return commentMapper.toDto(comment.getChildren());
    }

    public CommentDto getParentCommentByChildCommentId(Long childId, Long authUserId)
            throws ChildCommentNotFoundException, CommentIsNotChildrenException, UserNotFoundException {
        Comment comment = commentRepository
                .findById(childId)
                .orElseThrow(() -> new ChildCommentNotFoundException(childId));

        if (userService.isUserBannedByBannerUser(authUserId, comment.getUser().getId())) {
            throw new ChildCommentNotFoundException(childId);
        }

        if (comment.getParent() == null) {
            throw new CommentIsNotChildrenException(childId);
        }

        return commentMapper.toDto(comment.getParent());
    }

    public void deleteCommentById(Long id, Long authUserId)
            throws CommentNotFoundException, ForbiddenActionException, PostNotFoundException, UserNotFoundException {
        Comment comment = finderUtil.getCommentById(id);
        PostDto postDto = postService.getPostById(comment.getPost().getId(), authUserId);

        if (authUserId.equals(comment.getUser().getId()) || authUserId.equals(postDto.getUserId())) {
            commentRepository.delete(comment);
        } else {
            throw new ForbiddenActionException();
        }
    }

    public void updateCommentById(Long id, CommentDto commentDto, Long authUserId)
            throws CommentNotFoundException, ForbiddenActionException {
        Comment comment = finderUtil.getCommentById(id);

        if (!authUserId.equals(comment.getUser().getId())) {
            throw new ForbiddenActionException();
        }

        comment.setContent(commentDto.getContent());
        commentRepository.save(comment);
    }
}
