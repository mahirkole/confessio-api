package com.confessio.api.service.util;

import com.confessio.api.exception.*;
import com.confessio.api.model.*;
import com.confessio.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FinderUtil {

    private final CityRepository cityRepository;
    private final LocationRepository locationRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public FinderUtil(CityRepository cityRepository,
                      LocationRepository locationRepository,
                      UserRepository userRepository,
                      PostRepository postRepository,
                      CommentRepository commentRepository) {
        this.cityRepository = cityRepository;
        this.locationRepository = locationRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    public City getCityById(Long cityId) throws CityNotFoundException {
        return cityRepository.findById(cityId).orElseThrow(() -> new CityNotFoundException(cityId));
    }

    public Location getLocationById(Long locationId) throws LocationNotFoundException {
        return locationRepository.findById(locationId).orElseThrow(() -> new LocationNotFoundException(locationId));
    }

    public User getUserById(Long userId) throws UserNotFoundException {
        return userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
    }

    public Post getPostById(Long postId) throws PostNotFoundException {
        return postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException(postId));
    }

    public Comment getCommentById(Long commentId) throws CommentNotFoundException {
        return commentRepository.findById(commentId).orElseThrow(() -> new CommentNotFoundException(commentId));
    }

}
