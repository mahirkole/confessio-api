package com.confessio.api.mapper;

import com.confessio.api.dto.CommentDto;
import com.confessio.api.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {LocationMapper.class, UserMapper.class, PostMapper.class})
public abstract class CommentMapper {

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "postId", target = "post")
    @Mapping(source = "parentId", target = "parent")
    public abstract Comment toModel(CommentDto commentDto);

    @Mapping(source = "user", target = "userId")
    @Mapping(source = "post", target = "postId")
    @Mapping(source = "parent", target = "parentId")
    public abstract CommentDto toDto(Comment comment);

    public abstract List<Comment> toModel(List<CommentDto> commentDtoList);

    public abstract List<CommentDto> toDto(List<Comment> commentList);

    public Comment toModel(Long commentId) {
        if (commentId == null) {
            return null;
        }

        Comment comment = new Comment();
        comment.setId(commentId);
        return comment;
    }

    public Long toId(Comment comment) {
        if (comment == null) {
            return null;
        }

        return comment.getId();
    }
}
