package com.confessio.api.mapper;

import com.confessio.api.dto.HeartDto;
import com.confessio.api.model.Heart;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class, PostMapper.class})
public abstract class HeartMapper {

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "postId", target = "post")
    public abstract Heart toModel(HeartDto heartDto);

    @Mapping(source = "user", target = "userId")
    @Mapping(source = "post", target = "postId")
    public abstract HeartDto toDto(Heart heart);

    public abstract List<Heart> toModel(List<HeartDto> heartDtoList);

    public abstract List<HeartDto> toDto(List<Heart> heartList);

    public Heart toModel(Long id) {
        if (id == null) {
            return null;
        }

        Heart heart = new Heart();
        heart.setId(id);
        return heart;
    }

    public Long toId(Heart heart) {
        if (heart == null) {
            return null;
        }

        return heart.getId();
    }

}
