package com.confessio.api.mapper;

import com.confessio.api.dto.PostDto;
import com.confessio.api.model.Post;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {LocationMapper.class, UserMapper.class})
public abstract class PostMapper {

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "locationId", target = "location")
    public abstract Post toModel(PostDto postDto);

    @Mapping(source = "user", target = "userId")
    @Mapping(source = "location", target = "locationId")
    public abstract PostDto toDto(Post post);

    public Post toModel(Long postId) {
        if (postId == null) {
            return null;
        }

        Post post = new Post();
        post.setId(postId);
        return post;
    }

    public Long toId(Post post) {
        if (post == null) {
            return null;
        }

        return post.getId();
    }
}
