package com.confessio.api.mapper;

import com.confessio.api.dto.UserDto;
import com.confessio.api.model.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = BooleanMapper.class)
public abstract class UserMapper {

    public abstract User toModel(UserDto userDto);

    public abstract UserDto toDto(User user);

    public abstract List<User> toModel(List<UserDto> userDtoList);

    public abstract List<UserDto> toDto(List<User> userList);

    public User toModel(Long id) {
        if (id == null) {
            return null;
        }

        User user = new User();
        user.setId(id);
        return user;
    }

    public Long toId(User user) {
        if (user == null) {
            return null;
        }

        return user.getId();
    }
}
