package com.confessio.api.mapper;

import com.confessio.api.dto.CityDto;
import com.confessio.api.model.City;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class CityMapper {

    public abstract City toModel(CityDto cityDto);

    public abstract CityDto toDto(City city);

    public abstract List<City> toModel(List<CityDto> cityDto);

    public abstract List<CityDto> toDto(List<City> city);

    public City toModel(Long id) {
        if (id == null) {
            return null;
        }

        City city = new City();
        city.setId(id);
        return city;
    }

    public Long toId(City city) {
        if (city == null) {
            return null;
        }

        return city.getId();
    }
}
