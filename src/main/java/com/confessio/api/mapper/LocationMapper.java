package com.confessio.api.mapper;

import com.confessio.api.dto.LocationDto;
import com.confessio.api.model.Location;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CityMapper.class})
public abstract class LocationMapper {

    @Mapping(source = "cityId", target = "city")
    public abstract Location toModel(LocationDto locationDto);

    @Mapping(source = "city", target = "cityId")
    public abstract LocationDto toDto(Location location);

    public Location toModel(Long id) {
        if (id == null) {
            return null;
        }

        Location location = new Location();
        location.setId(id);
        return location;
    }

    public Long toId(Location location) {
        if (location == null) {
            return null;
        }

        return location.getId();
    }
}
