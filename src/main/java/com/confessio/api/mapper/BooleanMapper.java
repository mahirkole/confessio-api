package com.confessio.api.mapper;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public class BooleanMapper {

    Short toShort(Boolean _boolean) {
        if (_boolean) {
            return 1;
        } else {
            return 0;
        }
    }

    Boolean toBoolean(Short _short) {
        return _short == 1;
    }
}
