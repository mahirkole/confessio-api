package com.confessio.api.controller;

import com.confessio.api.config.Routes;
import com.confessio.api.dto.UserDto;
import com.confessio.api.exception.CannotBanYourselfException;
import com.confessio.api.exception.ForbiddenActionException;
import com.confessio.api.exception.UserNotFoundException;
import com.confessio.api.exception.UsernameIsTakenException;
import com.confessio.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Routes.User.ROOT)
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(Routes.User.ME)
    public ResponseEntity getMyProfile(Authentication auth) throws UserNotFoundException {
        UserDto user = userService.getUserByUsername(auth.getPrincipal().toString());
        return new ResponseEntity<>(user, HttpStatus.FOUND);
    }

    @GetMapping(Routes.User.GET)
    public ResponseEntity getUserById(@PathVariable Long id, Authentication auth) throws UserNotFoundException {
        Long authUserId = Long.parseLong(auth.getCredentials().toString());
        return new ResponseEntity<>(userService.getUserById(id, authUserId), HttpStatus.FOUND);
    }

    @PostMapping
    public ResponseEntity createUser(@RequestBody UserDto userDto) throws UsernameIsTakenException {
        return new ResponseEntity<>(userService.createUser(userDto), HttpStatus.CREATED);
    }

    @GetMapping(Routes.User.BAN)
    public ResponseEntity getBannedUsers(@RequestParam(name = "page", required = false, defaultValue = "0") int pageNumber,
                                         Authentication authentication,
                                         @PathVariable Long id) throws UserNotFoundException, ForbiddenActionException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());
        if (!id.equals(authUserId)) {
            throw new ForbiddenActionException();
        }
        return new ResponseEntity<>(userService.getBannedUsersOfUserId(id, pageNumber), HttpStatus.OK);
    }

    @PostMapping(Routes.User.BAN)
    public ResponseEntity banUnbanUser(@PathVariable Long id, Authentication authentication)
            throws UserNotFoundException, CannotBanYourselfException {
        userService.banUnbanUser(id, Long.parseLong(authentication.getCredentials().toString()));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity getAllUsers() {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

}
