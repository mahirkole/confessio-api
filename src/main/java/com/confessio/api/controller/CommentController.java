package com.confessio.api.controller;

import com.confessio.api.config.Routes;
import com.confessio.api.dto.CommentDto;
import com.confessio.api.exception.*;
import com.confessio.api.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Routes.Comment.ROOT)
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping(Routes.Comment.GET)
    public ResponseEntity getCommentById(@PathVariable Long id, Authentication authentication)
            throws CommentNotFoundException, UserNotFoundException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());
        return new ResponseEntity<>(commentService.getCommentById(id, authUserId), HttpStatus.FOUND);
    }

    @GetMapping(Routes.Comment.PARENT)
    public ResponseEntity getParentCommentByChildId(@PathVariable Long id, Authentication authentication)
            throws CommentIsNotChildrenException, ChildCommentNotFoundException, UserNotFoundException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());
        return new ResponseEntity<>(commentService.getParentCommentByChildCommentId(id, authUserId), HttpStatus.FOUND);
    }

    @GetMapping(Routes.Comment.CHILDREN)
    public ResponseEntity getChildrenCommentsByParentId(@PathVariable Long id, Authentication authentication)
            throws ParentCommentNotFoundException, CommentIsNotParentException, UserNotFoundException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());
        return new ResponseEntity<>(commentService.getChildren(id, authUserId), HttpStatus.OK);
    }

    @DeleteMapping(Routes.Comment.GET)
    public ResponseEntity deleteCommentById(@PathVariable Long id, Authentication authentication)
            throws CommentNotFoundException, ForbiddenActionException, PostNotFoundException, UserNotFoundException {
        commentService.deleteCommentById(id, Long.parseLong(authentication.getCredentials().toString()));
        return ResponseEntity.ok().build();
    }

    @PutMapping(Routes.Comment.GET)
    public ResponseEntity updateCommentById(@PathVariable Long id,
                                            @RequestBody CommentDto commentDto,
                                            Authentication authentication)
            throws CommentNotFoundException, ForbiddenActionException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());
        commentService.updateCommentById(id, commentDto, authUserId);
        return ResponseEntity.ok().build();
    }

}
