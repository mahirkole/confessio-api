package com.confessio.api.controller;

import com.confessio.api.config.Routes;
import com.confessio.api.dto.CommentDto;
import com.confessio.api.dto.PostDto;
import com.confessio.api.exception.*;
import com.confessio.api.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(Routes.Post.ROOT)
public class PostController {

    private PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public ResponseEntity getPosts(@RequestParam(name = "location", required = false) Long locationId,
                                   @RequestParam(name = "city", required = false) Long cityId,
                                   @RequestParam(name = "user", required = false) Long userId,
                                   @RequestParam(name = "page", required = false, defaultValue = "0") int pageNumber,
                                   Authentication authentication)
            throws LocationNotFoundException, CityNotFoundException, UserNotFoundException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());

        if (userId != null) {
            return new ResponseEntity<>(postService.getPostsByUserId(userId, authUserId, pageNumber), HttpStatus.OK);
        } else if (cityId != null) {
            return new ResponseEntity<>(postService.getPostsByCityId(cityId, authUserId, pageNumber), HttpStatus.OK);
        } else if (locationId != null) {
            return new ResponseEntity<>(
                    postService.getPostsByLocationId(locationId, authUserId, pageNumber), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(postService.getAll(authUserId, pageNumber), HttpStatus.OK);
        }
    }

    @GetMapping(Routes.Post.GET)
    public ResponseEntity getPostById(@PathVariable Long id, Authentication authentication)
            throws PostNotFoundException, UserNotFoundException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());
        return new ResponseEntity<>(postService.getPostById(id, authUserId), HttpStatus.FOUND);
    }

    @DeleteMapping(Routes.Post.GET)
    public ResponseEntity deletePostById(@PathVariable Long id, Authentication authentication)
            throws ForbiddenActionException, PostNotFoundException {
        postService.deletePostById(id, Long.parseLong(authentication.getCredentials().toString()));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(Routes.Post.Comment.ROOT)
    public ResponseEntity createComment(@PathVariable Long id, @RequestBody CommentDto commentDto, Authentication auth)
            throws ParentCommentNotFoundException, PostNotFoundException,
            PostForCommentIsInconsistentException, UserNotFoundException {
        commentDto.setPostId(id);
        commentDto.setUserId(Long.parseLong(auth.getCredentials().toString()));
        return new ResponseEntity<>(postService.createComment(commentDto), HttpStatus.CREATED);
    }

    @GetMapping(Routes.Post.Comment.ROOT)
    public ResponseEntity getCommentsByPostId(@PathVariable Long id,
                                              @RequestParam(name = "page", required = false, defaultValue = "0")
                                                      int pageNumber, Authentication authentication)
            throws PostNotFoundException, UserNotFoundException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());
        return new ResponseEntity<>(postService.getCommentsOfPostByPostId(id, authUserId, pageNumber), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createPost(@RequestBody @Valid PostDto postDto, Authentication auth)
            throws LocationNotFoundException, UserNotFoundException {
        postDto.setUserId(Long.parseLong(auth.getCredentials().toString()));
        return new ResponseEntity<>(postService.createPost(postDto), HttpStatus.CREATED);
    }

    @PostMapping(Routes.Post.Heart.ROOT)
    public ResponseEntity heartPost(@PathVariable Long id, Authentication authentication)
            throws PostNotFoundException, UserNotFoundException {
        return new ResponseEntity<>(postService.heartPost(id,
                Long.parseLong(authentication.getCredentials().toString())), HttpStatus.OK);
    }

    @PostMapping(Routes.Post.Heart.UNHEART)
    public ResponseEntity unheartPost(@PathVariable Long id, Authentication authentication)
            throws UserNotFoundException, PostNotFoundException, HeartNotFoundException {
        postService.unheartPost(id, Long.parseLong(authentication.getCredentials().toString()));
        return ResponseEntity.ok().build();
    }

    @GetMapping(Routes.Post.Heart.ROOT)
    public ResponseEntity getHeartsByPostId(@PathVariable Long id, Authentication authentication)
            throws PostNotFoundException, UserNotFoundException {
        Long authUserId = Long.parseLong(authentication.getCredentials().toString());
        return new ResponseEntity<>(postService.getHeartsOfPostByPostId(id, authUserId), HttpStatus.OK);
    }
}
