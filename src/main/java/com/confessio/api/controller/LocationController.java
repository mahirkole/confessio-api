package com.confessio.api.controller;

import com.confessio.api.config.Routes;
import com.confessio.api.dto.CityDto;
import com.confessio.api.dto.LocationDto;
import com.confessio.api.exception.CityNotFoundException;
import com.confessio.api.exception.LocationNotFoundException;
import com.confessio.api.service.LocationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(Routes.Location.ROOT)
public class LocationController {

    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping
    public ResponseEntity getLocations(@RequestParam(name = "city", required = false) Long cityId,
                                       @RequestParam(name = "page", required = false, defaultValue = "0") int pageNumber) {
        if (cityId != null) {
            return new ResponseEntity<>(locationService.getLocationsByCityId(cityId, pageNumber), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(locationService.getLocations(pageNumber), HttpStatus.OK);
        }
    }

    @PostMapping
    public ResponseEntity createLocation(@Valid @RequestBody LocationDto locationDto) throws CityNotFoundException {
        locationDto.setId(null);
        return new ResponseEntity<>(locationService.createLocation(locationDto), HttpStatus.CREATED);
    }

    @GetMapping(Routes.Location.GET)
    public ResponseEntity getLocationById(@PathVariable Long id) throws LocationNotFoundException {
        return new ResponseEntity<>(locationService.getLocationById(id), HttpStatus.FOUND);
    }

    @PutMapping(Routes.Location.GET)
    public ResponseEntity updateLocation(@PathVariable Long id, @RequestBody LocationDto locationDto)
            throws CityNotFoundException, LocationNotFoundException {
        locationDto.setId(id);
        return new ResponseEntity<>(locationService.updateLocation(locationDto), HttpStatus.OK);
    }

    @GetMapping(Routes.Location.City.ROOT)
    public ResponseEntity getCities(@RequestParam(name = "page", required = false, defaultValue = "0") int pageNumber) {
        return new ResponseEntity<>(locationService.getCities(pageNumber), HttpStatus.OK);
    }

    @GetMapping(Routes.Location.City.GET)
    public ResponseEntity getCityById(@PathVariable Long id) throws CityNotFoundException {
        return new ResponseEntity<>(locationService.getCityById(id), HttpStatus.FOUND);
    }

    @PutMapping(Routes.Location.City.GET)
    public ResponseEntity updateCity(@PathVariable Long id, @RequestBody CityDto cityDto) throws CityNotFoundException {
        cityDto.setId(id);
        return new ResponseEntity<>(locationService.updateCity(cityDto), HttpStatus.OK);
    }
}
