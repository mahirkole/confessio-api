package com.confessio.api.request;

import com.confessio.api.core.CoreRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class BanUnbanUserRequest extends CoreRequest {

    @NotNull
    private Long userId;
}
