package com.confessio.api.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {
    private String errorMessage;
    private Map<String, String> parameters = null;

    public Error() {
        this(null);
    }

    public Error(String errorMessage, String param, String value) {
        this(errorMessage);
        this.addParameter(param, value);
    }

    public Error(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void addParameter(String key, String value) {
        if (this.parameters == null) {
            this.parameters = new HashMap<>();
        }
        this.parameters.put(key, value);
    }
}
