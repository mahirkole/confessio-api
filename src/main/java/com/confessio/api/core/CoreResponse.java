package com.confessio.api.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreResponse {
    private Object data;
    private Boolean success = true;
    private List<Error> errorList = null;
    private Integer errorMessageSize = null;

    public CoreResponse(Object data) {
        this(data, true, null);
    }

    public CoreResponse(Boolean success, Error error) {
        this(null, success, Arrays.asList(error));
    }

    public CoreResponse(Boolean success, List<Error> errorList) {
        this(null, success, errorList);
    }

    private CoreResponse(Object data, Boolean success, List<Error> errorList) {
        this.data = data;
        this.success = success;
        this.errorList = errorList;

        if (errorList != null && errorList.size() > 0) {
            errorMessageSize = errorList.size();
        }
    }
}
