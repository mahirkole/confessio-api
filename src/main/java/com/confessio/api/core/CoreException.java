package com.confessio.api.core;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class CoreException extends Throwable {

    private Error error;

    public CoreException() {
    }

    public CoreException(String message) {
        this.error = new Error(message);
    }

    public CoreException(Error error) {
        this.error = error;
    }

    public CoreException(String message, String param, String value) {
        this.error = new Error(message, param, value);
    }

    public abstract ResponseEntity handle();

    public Error getError() {
        return this.error;
    }

    public ResponseEntity response(CoreException ex, HttpStatus httpStatus) {
        List<Error> errorList = new ArrayList<>();
        errorList.add(ex.getError());
        return new ResponseEntity<>(new CoreResponse(false, errorList), httpStatus);
    }
}
