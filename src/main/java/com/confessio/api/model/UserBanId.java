package com.confessio.api.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
public class UserBanId implements Serializable {

    @Column(name = "banned_id")
    private Long bannedId;

    @Column(name = "banner_id")
    private Long bannerId;

    private UserBanId() {

    }

    public UserBanId(Long bannerId, Long bannedId) {
        this.bannerId = bannerId;
        this.bannedId = bannedId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        UserBanId that = (UserBanId) obj;
        return Objects.equals(bannedId, that.bannedId) && Objects.equals(bannerId, that.bannerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bannedId, bannerId);
    }
}
