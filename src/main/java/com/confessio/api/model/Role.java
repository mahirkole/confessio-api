package com.confessio.api.model;

public enum Role {
    ADMIN, MOD, USER
}
