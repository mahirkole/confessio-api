package com.confessio.api.model;

import com.confessio.api.core.CoreModel;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "user_ban")
@Data
public class UserBan extends CoreModel {

    @EmbeddedId
    private UserBanId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("banner_id")
    private User banner;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("banned_id")
    private User banned;

    private UserBan() {

    }

    public UserBan(User banner, User banned) {
        this.id = new UserBanId(banner.getId(), banned.getId());
        this.banner = banner;
        this.banned = banned;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        UserBan that = (UserBan) obj;
        return Objects.equals(banner.getId(), that.getBanner().getId()) && Objects.equals(banned.getId(), that.getBanned().getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(banner.getId(), banned.getId());
    }

    @Override
    @Transient
    public UUID getUuid() {
        return super.getUuid();
    }
}
