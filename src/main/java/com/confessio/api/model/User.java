package com.confessio.api.model;

import com.confessio.api.core.CoreModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "users")
public class User extends CoreModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(unique = true)
    @Size(min = 4, max = 16)
    private String username;

    @NotBlank
    @Size(min = 8, max = 32)
    private String password;

    private Boolean isActive;

    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    /*
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_ban",
            joinColumns = @JoinColumn(name = "banner_id"),
            inverseJoinColumns = @JoinColumn(name = "banned_id"))
    private Set<User> bannedUsers = new HashSet<>();
    */

//    @ManyToMany(mappedBy = "bannedUsers")
//    private Set<User> banUsers = new HashSet<>();

    @OneToMany(
            mappedBy = "banner",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<UserBan> userBans = new ArrayList<>();

    public void banUser(User banned) {
        //this.bannedUsers.add(banned);
        this.userBans.add(new UserBan(this, banned));
    }

    public void unbanUser(User banned) {
        //this.bannedUsers.remove(banned);
        this.userBans.remove(new UserBan(this, banned));
    }
}
