package com.confessio.api.repository;

import com.confessio.api.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    //@Query("SELECT parent FROM Comment parent INNER JOIN FETCH parent.user INNER JOIN FETCH parent.children WHERE parent.post.id = :postId AND parent.parent IS NULL")
    @Query("SELECT c FROM Comment c WHERE c.post.id = :postId AND c.parent IS NULL")
    Page<Comment> getAllByPostId(Long postId, Pageable pageable);

}
