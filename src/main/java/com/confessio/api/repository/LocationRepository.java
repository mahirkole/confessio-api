package com.confessio.api.repository;

import com.confessio.api.model.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    @Query("SELECT l FROM Location l WHERE l.id = :id")
    Optional<Location> findById(Long id);

    @Query("SELECT l FROM Location l WHERE l.city.id = :cityId")
    Page<Location> findByCity(Long cityId, Pageable pageable);
}
