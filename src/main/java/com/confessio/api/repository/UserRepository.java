package com.confessio.api.repository;

import com.confessio.api.dto.UserDto;
import com.confessio.api.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findById(Long id);

    Optional<User> findByUsername(String username);

    //@Query("SELECT banned FROM User banner INNER JOIN banner.bannedUsers banned WHERE banner.id = :userId")
    @Query("SELECT ub.banned FROM User banner INNER JOIN banner.userBans ub WHERE banner.id = :userId")
    Page<User> findUsersBannedBy(Long userId, Pageable pageable);

    //@Query(name = "getAllUsers")
    //List<User> getAllUsers();
}
