package com.confessio.api.repository;

import com.confessio.api.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query("SELECT p FROM Post p INNER JOIN p.user u WHERE NOT EXISTS (SELECT 1 FROM UserBan ub WHERE ub.banner = u AND ub.id.bannedId = :authUserId) ORDER BY p.createdAt DESC")
    Page<Post> getAll(Long authUserId, Pageable pageable);

    @Query("SELECT p FROM Post p INNER JOIN FETCH p.location INNER JOIN FETCH p.user WHERE p.id = :id")
    Optional<Post> findById(Long id);

    @Query("SELECT p FROM Post p INNER JOIN p.user u WHERE p.location.id = :locationId AND NOT EXISTS (SELECT 1 FROM UserBan ub WHERE ub.banner = u AND ub.id.bannedId = :authUserId) ORDER BY p.createdAt DESC")
    Page<Post> getAllByLocationId(Long locationId, Long authUserId, Pageable pageable);

    @Query("SELECT p FROM Post p INNER JOIN p.user u WHERE p.location.city.id = :cityId AND NOT EXISTS (SELECT 1 FROM UserBan ub WHERE ub.banner = u AND ub.id.bannedId = :authUserId) ORDER BY p.createdAt DESC")
    Page<Post> getAllByCityId(Long cityId, Long authUserId, Pageable pageable);

    @Query("SELECT p FROM Post p WHERE p.user.id = :userId ORDER BY p.createdAt DESC")
    Page<Post> getAllByUserId(Long userId, Pageable pageable);
}
