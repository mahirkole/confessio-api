package com.confessio.api.repository;

import com.confessio.api.model.Heart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HeartRepository extends JpaRepository<Heart, Long> {

    @Query("SELECT h FROM Heart h INNER JOIN FETCH h.user WHERE h.post.id = :postId")
    List<Heart> getAllByPostId(Long postId);

    Optional<Heart> findByPostIdAndUserId(Long id, Long authUserId);
}
