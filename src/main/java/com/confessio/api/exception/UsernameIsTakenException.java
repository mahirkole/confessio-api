package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UsernameIsTakenException extends CoreException {
    public UsernameIsTakenException(String username) {
        super("This username is already taken", "username", username);
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.CONFLICT);
    }
}
