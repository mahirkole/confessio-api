package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class LocationNotFoundException extends CoreException {
    public LocationNotFoundException(Long locationId) {
        super("Location not found!", "locationId", locationId.toString());
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.NOT_FOUND);
    }
}
