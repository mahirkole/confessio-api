package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ParentCommentNotFoundException extends CoreException {
    public ParentCommentNotFoundException(Long parentId) {
        super("Parent comment not found!", "parentId", parentId.toString());
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.NOT_FOUND);
    }
}
