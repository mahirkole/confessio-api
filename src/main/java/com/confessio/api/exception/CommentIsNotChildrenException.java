package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CommentIsNotChildrenException extends CoreException {
    public CommentIsNotChildrenException(Long childId) {
        super("Comment is not a children comment", "childId", childId.toString());
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.BAD_REQUEST);
    }
}
