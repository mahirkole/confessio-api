package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ForbiddenActionException extends CoreException {
    public ForbiddenActionException() {
        super("Forbidden");
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.FORBIDDEN);
    }
}
