package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CommentIsNotParentException extends CoreException {
    public CommentIsNotParentException(Long parentId) {
        super("Comment is not a parent comment!", "parentId", parentId.toString());
    }
    
    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.BAD_REQUEST);
    }
}
