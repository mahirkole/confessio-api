package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ChildCommentNotFoundException extends CoreException {
    public ChildCommentNotFoundException(Long childId) {
        super("Child comment not found!", "childId", childId.toString());
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.NOT_FOUND);
    }
}
