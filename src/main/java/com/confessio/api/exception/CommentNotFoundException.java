package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CommentNotFoundException extends CoreException {
    public CommentNotFoundException(Long id) {
        super("Comment not found!", "id", id.toString());
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.NOT_FOUND);
    }
}
