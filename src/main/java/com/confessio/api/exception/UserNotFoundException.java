package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UserNotFoundException extends CoreException {
    public UserNotFoundException(Long id) {
        super("User not found", "userId", id.toString());
    }

    public UserNotFoundException(String username) {
        super("User not found", "username", username);
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.NOT_FOUND);
    }
}
