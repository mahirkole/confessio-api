package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class HeartNotFoundException extends CoreException {
    public HeartNotFoundException() {
        super("Heart not found!");
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.NOT_FOUND);
    }
}
