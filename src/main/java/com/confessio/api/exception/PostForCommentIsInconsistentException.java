package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class PostForCommentIsInconsistentException extends CoreException {
    public PostForCommentIsInconsistentException(Long postId) {
        super("Comment post is not the same as parent comment post!", "postId", postId.toString());
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.BAD_REQUEST);
    }
}
