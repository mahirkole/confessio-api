package com.confessio.api.exception;

import com.confessio.api.core.CoreException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CannotBanYourselfException extends CoreException {
    public CannotBanYourselfException() {
        super("You can not ban yourself!");
    }

    @Override
    public ResponseEntity handle() {
        return response(this, HttpStatus.BAD_REQUEST);
    }
}
