package com.confessio.api.util;

import com.confessio.api.core.CoreException;

public interface BannedUserStrategy {
    void handle(Long parameter, Long authUserId) throws CoreException;
}
