package com.confessio.api.util;

import com.confessio.api.core.CoreException;
import com.confessio.api.exception.PostNotFoundException;
import com.confessio.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component
public class BannedUserPostAccessStrategy implements BannedUserStrategy {

    private final UserService userService;

    @Autowired
    public BannedUserPostAccessStrategy(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void handle(Long parameter, Long authUserId) throws CoreException {
        if (userService.isUserBannedByBannerUser(authUserId, parameter)) {
            throw new PostNotFoundException(parameter);
        }
    }
}
