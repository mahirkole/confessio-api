package com.confessio.api.config.auth;

import com.confessio.api.core.CoreResponse;
import com.confessio.api.model.User;
import com.confessio.api.util.UserDetailsImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.confessio.api.config.Constants.*;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    @Autowired
    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        User user;

        try {
            user = new ObjectMapper().readValue(request.getInputStream(), User.class);
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain,
            Authentication authentication) throws IOException {
        String jwtToken =
                Jwts.builder()
                        .setSubject(((UserDetailsImpl) authentication.getPrincipal()).getUsername())
                        .setExpiration(new Date(System.currentTimeMillis() + JWT_EXP_TIME_IN_MILLIS))
                        .signWith(SignatureAlgorithm.HS384, JWT_SECRET.getBytes())
                        .compact();

        response.addHeader(JWT_HEADER, JWT_TOKEN_PREFIX.concat(jwtToken));
        response.addHeader("Content-Type", "application/json;charset=UTF-8");
        response.getWriter().write("{\"success\": true, \"token\": \"" + jwtToken + "\"}");
        response.setStatus(200);
    }
}
