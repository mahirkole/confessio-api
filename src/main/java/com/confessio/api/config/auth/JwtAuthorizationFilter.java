package com.confessio.api.config.auth;

import com.confessio.api.config.Constants;
import com.confessio.api.service.UserService;
import com.confessio.api.util.UserDetailsImpl;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private UserService userService;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, UserService userService) {
        super(authenticationManager);
        this.userService = userService;
    }

    protected void doFilterInternal(
            HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String headerValue = request.getHeader(Constants.JWT_HEADER);

        if (headerValue == null || !headerValue.startsWith(Constants.JWT_TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        Authentication authentication = null;

        String username =
                Jwts.parser()
                        .setSigningKey(Constants.JWT_SECRET.getBytes())
                        .parseClaimsJws(headerValue.replace(Constants.JWT_TOKEN_PREFIX, ""))
                        .getBody()
                        .getSubject();

        if (username != null) {
            UserDetailsImpl authenticatedUser = (UserDetailsImpl) userService.loadUserByUsername(username);
            authentication = new UsernamePasswordAuthenticationToken(
                    authenticatedUser.getUsername(),
                    authenticatedUser.getUser().getId(),
                    authenticatedUser.getAuthorities());
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }
}
