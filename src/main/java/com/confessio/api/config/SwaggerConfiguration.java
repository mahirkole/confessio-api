package com.confessio.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(Arrays.asList(
                        new ParameterBuilder()
                                .name("Authentication")
                                .description("Header that used to authenticate with JWT")
                                .modelRef(new ModelRef("string"))
                                .parameterType("header")
                                .required(true).build()))
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant(Routes.API_ROOT + "/**"))
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo("Confessio-API",
                "Confessio API is a RESTful api for Confessio Project",
                "v.0.3.1",
                null,
                new Contact("Mahir Köle", null, "mahirkole@gmail.com"),
                "Do What the Fuck You Want to Public License",
                "http://www.wtfpl.net/", Collections.emptyList());
    }

}
