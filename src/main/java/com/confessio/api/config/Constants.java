package com.confessio.api.config;

public class Constants {
    public static final long JWT_EXP_TIME_IN_MILLIS = 10000 * 60 * 60 * 24 * 30;
    public static final String JWT_SECRET = "KsCSdhxW9XX7jHGkv11mZB3pgagmSVT4KsCSdhxW9XX7jHGkv11mZB3pgagmSVT4";
    public static final String JWT_HEADER = "Authorization";
    public static final String JWT_TOKEN_PREFIX = "Bearer ";
}
