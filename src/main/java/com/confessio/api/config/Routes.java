package com.confessio.api.config;

public class Routes {

    public static final String API_ROOT = "/v1";

    public static class User {
        public static final String ROOT = Routes.API_ROOT + "/user";
        public static final String GET = "/{id}";
        public static final String ME = "/me";
        public static final String BAN = GET + "/ban";
    }

    public static class Post {
        public static final String ROOT = Routes.API_ROOT + "/post";
        public static final String GET = "/{id}";

        public static class Comment {
            public static final String ROOT = Post.GET + "/comment";
        }

        public static class Heart {
            public static final String ROOT = Post.GET + "/heart";
            public static final String UNHEART = Post.GET + "/unheart";
        }
    }

    public static class Comment {
        public static final String ROOT = Routes.API_ROOT + "/comment";
        public static final String GET = "/{id}";
        public static final String PARENT = GET + "/parent";
        public static final String CHILDREN = GET + "/children";
    }

    public static class Heart {
        public static final String ROOT = Routes.API_ROOT + "/heart";
        public static final String GET = ROOT + "/{id}";
        public static final String USER = GET + "/user";
    }

    public static class Location {
        public static final String ROOT = Routes.API_ROOT + "/location";
        public static final String GET = "/{id}";

        public class City {
            public static final String ROOT = "/city";
            public static final String GET = ROOT + "/{id}";
        }
    }
}
